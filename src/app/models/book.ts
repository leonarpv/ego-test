export class Book {
  nombre: string;
  editorial: string;
  autor: string;
  edicion: number;
}

