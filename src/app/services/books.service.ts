import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs'



@Injectable()
export class BooksService {

  constructor( private http: HttpClient) { }

  getBooks(){
    return this.http.get("./assets/books.json");
  }


}
