import { Component, OnInit,Input,Output } from '@angular/core';
import { Book } from './../../../models/book';
import { NumbersDirective } from './../../../directives/numbers.directive';
import { BooksService } from './../../../services/books.service';
import { EventEmitter } from 'events';
@Component({
  selector: 'app-manage-book',
  templateUrl: './manage-book.component.html',
  styleUrls: ['./manage-book.component.css']
})
export class ManageBookComponent implements OnInit {
  @Input()  booknew:Book[];
  public formData:FormData;
  public book:Book =new Book();
  submitted = false;
  constructor(
  ) { }

  ngOnInit() {
  }
  onSubmit(book) {
    this.booknew=book;
  }

}
