import { Component, OnInit,Input,Output,ViewChild, AfterViewInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs'
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';
import { ManageBookComponent } from '../book/manage-book/manage-book.component';
import { Book } from './../../models/book';
import { BooksService } from './../../services/books.service';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})

export class BooksComponent implements OnInit {
  displayedColumnsBooks = ['nombre', 'editorial', 'autor', 'edicion', 'opciones'];
  public BOOKS: Books[] = [];
  public dataSource: MatTableDataSource<Book> = new MatTableDataSource();
  @ViewChild(ManageBookComponent) manage;
  constructor(
    public booksService: BooksService,
    public dialog: MatDialog
  ) { }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      height: '400px',
      width: '400px',
    });
  }


  ngOnInit() {
    this.listarLibros();
  }

  listarLibros() {
    this.booksService.getBooks().subscribe(data => {
      this.BOOKS = data;
      this.dataSource = new MatTableDataSource(this.BOOKS);
    }, err => {
    })
  }

  addBook(book){
    this.BOOKS.push(this.manage.booknew)
  }
  deleteBook(book) {
    this.BOOKS.splice(book, 1);
    this.dataSource = new MatTableDataSource(this.BOOKS);
  }

}

export interface Books {
  nombre: string;
  editorial: string;
  autor: string;
  edicion: number;
}


