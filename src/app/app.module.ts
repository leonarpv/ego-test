import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule }   from '@angular/forms';
// Material
import {
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatToolbarModule,
  MatDialogModule,
  MatIconModule
} from '@angular/material';
// Servicios
import { BooksService } from './services/books.service';
import { CdkTableModule } from '@angular/cdk/table';
// Componentes
import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ModalComponent } from './components/modal/modal.component';
import { ManageBookComponent } from './components/book/manage-book/manage-book.component';
import { NumbersDirective } from './directives/numbers.directive';






@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavbarComponent,
    ModalComponent,
    ManageBookComponent,
    NumbersDirective,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    CdkTableModule
  ],
  entryComponents:[ModalComponent],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
